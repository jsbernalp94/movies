package co.jonathanbernal.domain.repository


import co.jonathanbernal.domain.entities.ShoppingCart
import io.reactivex.Observable


interface IShoppingCartRepository {
    //INSERT
    fun insertMovieToShoppingCar(shoppingCart: ShoppingCart)
    //GET
    fun getMovieListFromShoppingCar(): Observable<List<ShoppingCart>>
    //DELETE
    fun deleteMovieFromShoppingCart(idMovie: Int)
    fun deleteAllMoviesFromShoppingCart()
}