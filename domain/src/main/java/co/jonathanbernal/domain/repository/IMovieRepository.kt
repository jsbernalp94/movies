package co.jonathanbernal.domain.repository

import co.jonathanbernal.domain.entities.Movie
import io.reactivex.Observable

interface IMovieRepository {
    //GET
    fun getMovieList(page: Int):Observable<List<Movie>>
    fun getMovieListfromDb():Observable<List<Movie>>
    fun getMovie(idMovie: Int) : Observable<List<Movie>>

    //INSERT
    fun insertMovieToDb(movie: Movie)

    //UPDATE
    fun updateMovie(id:Int,isInShoppingCart: Boolean)
    fun updateAllMovies(isInShoppingCart: Boolean)

}