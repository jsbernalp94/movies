package co.jonathanbernal.domain.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

data class MovieResponse(val results: List<Movie>)

@Entity(
    tableName = "Movie"
)
data class Movie(
    @Json(name = "id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id : Int,

    @Json(name = "backdrop_path")
    @ColumnInfo(name = "backdrop_path")
    val backdrop_path: String?,

    @Json(name = "original_title")
    @ColumnInfo(name = "original_title")
    val original_title: String,

    @Json(name = "overview")
    @ColumnInfo(name = "overview")
    val overview: String,

    @Json(name = "poster_path")
    @ColumnInfo(name = "poster_path")
    val poster_path: String,

    @ColumnInfo(name = "isInShoppingCart")
    val isInShoppingCart: Boolean
)