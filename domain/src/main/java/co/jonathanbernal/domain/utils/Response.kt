package co.jonathanbernal.domain.utils

sealed class Response {
    data class Success(val data: Any) : Response()
    data class Failure(val throwable: Throwable) : Response()
}