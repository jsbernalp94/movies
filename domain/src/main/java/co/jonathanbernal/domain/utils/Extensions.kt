package co.jonathanbernal.domain.utils

import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.entities.ShoppingCart

fun Movie.toShoppingCar(): ShoppingCart {
    return ShoppingCart(
        id,
        poster_path,
        original_title
    )
}