package co.jonathanbernal.domain.usecase.insert.shopping_car

import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.repository.IMovieRepository
import co.jonathanbernal.domain.repository.IShoppingCartRepository
import co.jonathanbernal.domain.utils.toShoppingCar
import javax.inject.Inject

class InsertShoppingCarUseCase @Inject constructor(
    private val iShoppingCartRepository: IShoppingCartRepository,
    private val iMovieRepository: IMovieRepository
) {

    fun insertMovieToShoppingCar(movie: Movie) {
        iMovieRepository.updateMovie(movie.id,true)
        iShoppingCartRepository.insertMovieToShoppingCar(movie.toShoppingCar())
    }

}