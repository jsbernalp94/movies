package co.jonathanbernal.domain.usecase.delete.shoppingCar

import co.jonathanbernal.domain.repository.IMovieRepository
import co.jonathanbernal.domain.repository.IShoppingCartRepository
import javax.inject.Inject

class DeleteShoppingCartUseCase @Inject constructor(
    private val iShoppingCartRepository: IShoppingCartRepository,
    private val iMovieRepository: IMovieRepository
) {

    fun deleteMovieFromShoppingCart(idMovie: Int){
        iMovieRepository.updateMovie(idMovie,false)
        iShoppingCartRepository.deleteMovieFromShoppingCart(idMovie)
    }

    fun deleteAllMovies(){
        iMovieRepository.updateAllMovies(false)
        iShoppingCartRepository.deleteAllMoviesFromShoppingCart()
    }

}