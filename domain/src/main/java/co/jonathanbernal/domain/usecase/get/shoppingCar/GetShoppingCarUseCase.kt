package co.jonathanbernal.domain.usecase.get.shoppingCar



import co.jonathanbernal.domain.entities.ShoppingCart
import co.jonathanbernal.domain.repository.IShoppingCartRepository
import co.jonathanbernal.domain.utils.Response
import io.reactivex.Observable
import javax.inject.Inject

class GetShoppingCarUseCase@Inject constructor(
    private val iShoppingCartRepository: IShoppingCartRepository
) {

    fun getMovieListFromShoppingCar(): Observable<Response> {
        return iShoppingCartRepository.getMovieListFromShoppingCar()
            .map { Response.Success(it) as Response }
            .onErrorReturn { Response.Failure(it) }
    }

}