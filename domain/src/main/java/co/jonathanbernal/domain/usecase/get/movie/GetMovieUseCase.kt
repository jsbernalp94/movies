package co.jonathanbernal.domain.usecase.get.movie

import co.jonathanbernal.domain.repository.IMovieRepository
import co.jonathanbernal.domain.utils.Response
import io.reactivex.Observable
import javax.inject.Inject

class GetMovieUseCase @Inject constructor(
    private val iMovieRepository: IMovieRepository
) {

    fun downloadMovieList(page: Int): Observable<Response> {
        return iMovieRepository.getMovieList(page)
            .map { Response.Success(it) as Response }
            .onErrorReturn { Response.Failure(it) }
    }

    fun getMovieList(): Observable<Response> {
        return iMovieRepository.getMovieListfromDb()
            .map { Response.Success(it) as Response }
            .onErrorReturn { Response.Failure(it) }
    }


    fun getMovie(idMovie: Int): Observable<Response> {
        return iMovieRepository.getMovie(idMovie)
            .flatMap { listMovie ->
                if (listMovie.size == 1) {
                    Observable.just(Response.Success(listMovie[0]))
                } else {
                    Observable.just(Response.Failure(Throwable("Error en la consulta")))
                }
            }
    }


}