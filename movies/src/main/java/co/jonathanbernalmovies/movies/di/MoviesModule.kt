package co.jonathanbernalmovies.movies.di

import co.jonathanbernalmovies.movies.view.MoviesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MoviesModule {

    @ContributesAndroidInjector
    abstract fun providesMoviesFragment():MoviesFragment
}