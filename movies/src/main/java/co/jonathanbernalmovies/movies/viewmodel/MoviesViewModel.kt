package co.jonathanbernalmovies.movies.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.usecase.delete.shoppingCar.DeleteShoppingCartUseCase
import co.jonathanbernal.domain.usecase.get.movie.GetMovieUseCase
import co.jonathanbernal.domain.usecase.insert.shopping_car.InsertShoppingCarUseCase
import co.jonathanbernal.domain.utils.Response
import co.jonathanbernalcommon.common.utils.PreferenceConstants
import co.jonathanbernalcommon.common.utils.SharedPreferences
import co.jonathanbernalcommon.common.utils.addTo
import co.jonathanbernalmovies.movies.R
import co.jonathanbernalmovies.movies.view.MoviesAdapter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoviesViewModel  @Inject constructor(
    private val getMovieUseCase: GetMovieUseCase,
    private val insertShoppingCarUseCase: InsertShoppingCarUseCase,
    private val deleteShoppingCartUseCase: DeleteShoppingCartUseCase,
    private val sharedPreferences: SharedPreferences
) : ViewModel() {

    var movies: MutableLiveData<List<Movie>> = MutableLiveData()
    var idMovie: MutableLiveData<Int> = MutableLiveData()
    var error: MutableLiveData<Int> = MutableLiveData()
    var listAdapter: MoviesAdapter? = null
    var isDownloading = false
    private val compositeDisposable = CompositeDisposable()

    companion object {
        private val TAG = MoviesViewModel::class.java.simpleName
        private const val PAGE_SIZE = 20
    }

    fun downloadMovieList() {
        if (!isDownloading) {
            isDownloading = true
            val page = sharedPreferences.getInt(PreferenceConstants.PAGE)
            val pageToDownload = if (page < 1) 1 else page + 1
            getMovieUseCase.downloadMovieList(pageToDownload)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe{ manageResponseApi(it,pageToDownload) }
                    .addTo(compositeDisposable)
        }
    }

    private fun manageResponseApi(response: Response, pageToDownload: Int) {
        when (response) {
            is Response.Success -> sharedPreferences.setInt(PreferenceConstants.PAGE, pageToDownload)
            is Response.Failure -> error.postValue(R.string.error)
        }
    }

    fun getMovieListFromDb(){
        getMovieUseCase.getMovieList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { manageResponseFromDb(it)}
            .addTo(compositeDisposable)
    }

    private fun manageResponseFromDb(response: Response) {
        when (response) {
            is Response.Failure -> {
                error.postValue(R.string.error_db)
            }
            is Response.Success -> {
                movies.postValue(response.data as List<Movie>)
                isDownloading = false
                if ((response.data as List<Movie>).isEmpty()){
                    downloadMovieList()
                }
            }
        }
    }

    fun setData(list: List<Movie>) {
        listAdapter?.setMovieList(list)
    }

    fun getMovie(position: Int): Movie?{
        val movies: MutableLiveData<List<Movie>> = movies
        return movies.value?.get(position)
    }

    fun getRecyclerMovieAdapter():MoviesAdapter?{
        listAdapter = MoviesAdapter(this, R.layout.cell_movie)
        return listAdapter
    }

    fun onLoadMoreData(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if (!isInFooter(visibleItemCount,firstVisibleItemPosition,totalItemCount)){
            return
        }
        downloadMovieList()
    }

    fun openMovieDetail(position: Int){
        idMovie.postValue((movies.value)?.get(position)?.id)
    }

    fun addShoppingCar(position: Int) {
        Completable.fromAction {
            val movie = (movies.value)?.get(position)
            movie?.let { insertShoppingCarUseCase.insertMovieToShoppingCar(it) }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe().addTo(compositeDisposable)
    }

    fun deleteMovieFromShoppingCart(position: Int){
        Completable.fromAction {
            val movie = (movies.value)?.get(position)
            movie?.let { deleteShoppingCartUseCase.deleteMovieFromShoppingCart(movie.id)}
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe().addTo(compositeDisposable)
    }


    private fun isInFooter(
        visibleItemCount: Int,
        firstVisibleItemPosition: Int,
        totalItemCount: Int
    ): Boolean {
        return visibleItemCount + firstVisibleItemPosition >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= PAGE_SIZE
    }


}
