package co.jonathanbernalmovies.movies.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class MoviesViewModelFactory @Inject constructor(private val moviesViewModel: MoviesViewModel) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(moviesViewModel::class.java)) {
            return moviesViewModel as T
        }
        throw IllegalArgumentException("uknown class name")
    }
}