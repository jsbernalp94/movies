package co.jonathanbernalmovies.movies.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.jonathanbernalcommon.common.utils.toast
import co.jonathanbernalmovies.movies.R
import co.jonathanbernalmovies.movies.databinding.FragmentMoviesBinding
import co.jonathanbernalmovies.movies.viewmodel.MoviesViewModel
import co.jonathanbernalmovies.movies.viewmodel.MoviesViewModelFactory
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class MoviesFragment : Fragment() {
    @Inject
    lateinit var moviesViewModelFactory: MoviesViewModelFactory

    @Inject
    lateinit var moviesViewModel: MoviesViewModel

    lateinit var binding: FragmentMoviesBinding


    private val onScrollListener: RecyclerView.OnScrollListener by lazy {
        object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val visibleItemCount: Int = layoutManager.childCount
                val totalItemCount: Int = layoutManager.itemCount
                val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

                moviesViewModel.onLoadMoreData(visibleItemCount, firstVisibleItemPosition, totalItemCount)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_movies,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AndroidSupportInjection.inject(this)
        moviesViewModel = ViewModelProvider(this, moviesViewModelFactory).get(moviesViewModel::class.java)
        binding.movieList = moviesViewModel
        binding.recyclerviewMovies.run {
            addOnScrollListener(onScrollListener)
        }
        moviesViewModel.getMovieListFromDb()
        moviesViewModel.movies.observe(viewLifecycleOwner,{
            moviesViewModel.setData(it)
        })

        moviesViewModel.idMovie.observe(viewLifecycleOwner,{idMovie->
            val bundle = bundleOf("idMovie" to idMovie)
            findNavController().navigate(R.id.action_navigation_movies_to_navigation_movie_detail,bundle)
        })

        moviesViewModel.error.observe(viewLifecycleOwner,{
            context?.toast(it)
        })

    }


}