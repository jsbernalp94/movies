package co.jonathanbernalmovies.movies.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernalmovies.movies.viewmodel.MoviesViewModel

class MoviesAdapter internal constructor(var moviesViewModel: MoviesViewModel, var resource: Int) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    private var movieListAdapter: List<Movie> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setMoviesCard(moviesViewModel, position)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    private fun getLayoutIdForPosition(position: Int): Int {
        return resource
    }

    override fun getItemCount(): Int {
        return movieListAdapter.size
    }

    fun setMovieList(movies: List<Movie>) {
        this.movieListAdapter = movies
        notifyDataSetChanged()
    }

    class ViewHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        private var binding: ViewDataBinding? = null

        init {
            this.binding = binding
        }

        fun setMoviesCard(moviesViewModel: MoviesViewModel, position: Int) {
            binding?.setVariable(BR.itemMovie, moviesViewModel)
            binding?.setVariable(BR.position, position)
            binding?.executePendingBindings()
        }

    }
}