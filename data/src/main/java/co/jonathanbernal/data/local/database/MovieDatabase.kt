package co.jonathanbernal.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import co.jonathanbernal.data.local.dao.MovieDao
import co.jonathanbernal.data.local.dao.ShoppingCartDao
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.entities.ShoppingCart

@Database(entities = [Movie::class, ShoppingCart::class],version = 1)
abstract class MovieDatabase: RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun shoppingCartDao(): ShoppingCartDao
}