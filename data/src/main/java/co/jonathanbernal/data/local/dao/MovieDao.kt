package co.jonathanbernal.data.local.dao

import androidx.room.*
import co.jonathanbernal.domain.entities.Movie
import io.reactivex.Observable

@Dao
interface MovieDao {

    @Query("SELECT * FROM Movie")
    fun getAllMovies():Observable<List<Movie>>

    @Insert(
        onConflict = OnConflictStrategy.REPLACE
    )
    fun insertMovie(movie: Movie)

    @Query("SELECT * FROM Movie WHERE id= :idMovie")
    fun getMovie(idMovie: Int):Observable<List<Movie>>

    @Query("UPDATE Movie SET isInShoppingCart= :isInShoppingCart WHERE id = :id")
    fun updateMovie(id: Int, isInShoppingCart: Boolean)

    @Query("UPDATE Movie SET isInShoppingCart = :inShoppingCart")
    fun updateAllMovies(inShoppingCart: Boolean)
}