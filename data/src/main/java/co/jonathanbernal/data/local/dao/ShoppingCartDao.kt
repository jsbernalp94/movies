package co.jonathanbernal.data.local.dao

import androidx.room.*
import co.jonathanbernal.domain.entities.ShoppingCart
import io.reactivex.Observable


@Dao
interface ShoppingCartDao {

    @Insert(
        onConflict = OnConflictStrategy.IGNORE
    )
    fun insertMovieToCar(shoppingCart: ShoppingCart)

    @Query("SELECT * FROM ShoppingCart")
    fun getMovieFromShoppingCar():Observable<List<ShoppingCart>>

    @Query("DELETE FROM shoppingcart WHERE id = :idMovie")
    fun deleteMovieFromShoppingCart(idMovie: Int)
    @Query("DELETE FROM ShoppingCart")
    fun deleteAllMoviesFromShoppingCart()

}