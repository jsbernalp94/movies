package co.jonathanbernal.data.repository

import co.jonathanbernal.data.local.dao.ShoppingCartDao
import co.jonathanbernal.domain.entities.ShoppingCart
import co.jonathanbernal.domain.repository.IShoppingCartRepository
import io.reactivex.Observable

class ShoppingCartRepositoryImplementation(
    private val shoppingCartDao: ShoppingCartDao
) : IShoppingCartRepository {

    override fun insertMovieToShoppingCar(shoppingCart: ShoppingCart) {
       shoppingCartDao.insertMovieToCar(shoppingCart)
    }

    override fun getMovieListFromShoppingCar(): Observable<List<ShoppingCart>> {
        return shoppingCartDao.getMovieFromShoppingCar()
    }

    override fun deleteMovieFromShoppingCart(idMovie: Int) {
        shoppingCartDao.deleteMovieFromShoppingCart(idMovie)
    }

    override fun deleteAllMoviesFromShoppingCart() {
        shoppingCartDao.deleteAllMoviesFromShoppingCart()
    }

}