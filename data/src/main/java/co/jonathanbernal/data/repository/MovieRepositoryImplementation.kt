package co.jonathanbernal.data.repository

import co.jonathanbernal.data.endpoint.MovieApi
import co.jonathanbernal.data.local.dao.MovieDao
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.data.mapper.MovieMapper
import co.jonathanbernal.domain.repository.IMovieRepository
import io.reactivex.Observable

class MovieRepositoryImplementation(
    private val movieApi: MovieApi,
    private val movieMapper: MovieMapper,
    private val movieDao: MovieDao
): IMovieRepository {

    override fun getMovieList(page: Int): Observable<List<Movie>> {
        return movieApi.getMovieList(page)
            .map { movieMapper.map(it) }
            .doOnNext {listMovie-> listMovie.forEach{ movie ->
                insertMovieToDb(movie)}
            }
    }

    override fun getMovieListfromDb(): Observable<List<Movie>> {
        return movieDao.getAllMovies()
    }

    override fun getMovie(idMovie: Int): Observable<List<Movie>> {
        return movieDao.getMovie(idMovie)
    }

    override fun insertMovieToDb(movie: Movie) {
        movieDao.insertMovie(movie)
    }

    override fun updateMovie(id: Int, isInShoppingCart: Boolean) {
        movieDao.updateMovie(id,isInShoppingCart)
    }

    override fun updateAllMovies(isInShoppingCart: Boolean) {
        movieDao.updateAllMovies(isInShoppingCart)
    }


}