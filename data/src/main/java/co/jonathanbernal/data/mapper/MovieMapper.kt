package co.jonathanbernal.data.mapper

import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.entities.MovieResponse
import javax.inject.Inject

class MovieMapper @Inject constructor() {

    fun map(responseList : MovieResponse): List<Movie>{
        return responseList.results
    }

}