package co.jonathanbernal.data.endpoint

import co.jonathanbernal.data.service.MovieService
import co.jonathanbernal.domain.entities.MovieResponse
import io.reactivex.Observable
import javax.inject.Inject


class MovieApi @Inject constructor(private val movieService: MovieService) {

    fun getMovieList(page:Int): Observable<MovieResponse> {
        return movieService.getMovieList(page)
    }
}