package co.jonathanbernal.data.service

import co.jonathanbernal.domain.entities.MovieResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieService {

    @GET("movie/popular")
    fun getMovieList(
        @Query("page") numberPath: Int ):Observable<MovieResponse>

}