package co.jonathanbernal.data.repository


import co.jonathanbernal.data.endpoint.MovieApi
import co.jonathanbernal.data.local.dao.MovieDao
import co.jonathanbernal.data.mapper.MovieMapper
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.entities.MovieResponse
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieRepositoryImplementationTest {

    lateinit var movieResponseImplementation: MovieRepositoryImplementation
    @Mock
    lateinit var movieMapper: MovieMapper
    @Mock
    lateinit var movieApi: MovieApi
    @Mock
    lateinit var movieDao: MovieDao
    @Mock
    lateinit var movieResponse: MovieResponse
    @Mock
    lateinit var listMovie: List<Movie>
    private lateinit var movieTest: Movie

    @Before
    fun setUp() {
        movieResponseImplementation = MovieRepositoryImplementation(movieApi, movieMapper, movieDao)
        movieTest = Movie(4567, "testImage.png", "Movie Test", "this is a Movie Test", "posterImageTest.jpg", true)
    }


    @Test
    fun `retrieves the movie list`() {
        given(movieApi.getMovieList(any())).willReturn(Observable.just(movieResponse))
        given(movieMapper.map(movieResponse)).willReturn(listMovie)
        movieResponseImplementation.getMovieList(1).test()
        verify(movieApi).getMovieList(any())

    }

    @Test
    fun `getMovieList returns error on failure`() {
        val error = Throwable()
        given(movieApi.getMovieList(any())).willReturn(Observable.error(error))
        movieResponseImplementation.getMovieList(156).test().assertError(error)
    }

    @Test
    fun `get Movie by id`(){
        given(movieDao.getMovie(any())).willReturn(Observable.just(mock()))
        movieResponseImplementation.getMovie(4523).test()
        verify(movieDao).getMovie(any())
    }


    @Test
    fun `insert Movie In Table Movie`(){
        movieResponseImplementation.insertMovieToDb(movieTest)
        verify(movieDao).insertMovie(any())
    }

    @Test
    fun `update Movie In Table Movie`(){
        movieResponseImplementation.updateMovie(2,false)
        verify(movieDao).updateMovie(any(), any())
    }

    @Test
    fun `update all movies to isInShoppingCart equals true`(){
        movieResponseImplementation.updateAllMovies(true)
        verify(movieDao).updateAllMovies(any())
    }

    @Test
    fun `update all movies to isInShoppingCart equals false`(){
        movieResponseImplementation.updateAllMovies(false)
        verify(movieDao).updateAllMovies(any())
    }

}