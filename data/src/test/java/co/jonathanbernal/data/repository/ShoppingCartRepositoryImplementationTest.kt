package co.jonathanbernal.data.repository

import co.jonathanbernal.data.local.dao.ShoppingCartDao
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.utils.toShoppingCar
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ShoppingCartRepositoryImplementationTest {

    @Mock
    lateinit var shoppingCartDao: ShoppingCartDao


    lateinit var shoppingCartRepositoryImplementation: ShoppingCartRepositoryImplementation
    private lateinit var movie: Movie

    @Before
    fun setUp(){
        shoppingCartRepositoryImplementation = ShoppingCartRepositoryImplementation(shoppingCartDao)
        movie = Movie(4567, "testImage.png", "Movie Test", "this is a Movie Test", "posterImageTest.jpg", true)
    }

    @Test
    fun `insert Movie To ShoppingCar`() {
        shoppingCartRepositoryImplementation.insertMovieToShoppingCar(movie.toShoppingCar())
        verify(shoppingCartDao).insertMovieToCar(any())
    }

    @Test
    fun `get MovieList From ShoppingCar`() {
        given(shoppingCartDao.getMovieFromShoppingCar()).willReturn(Observable.just(mock()))
        shoppingCartRepositoryImplementation.getMovieListFromShoppingCar()
        verify(shoppingCartDao).getMovieFromShoppingCar()

    }

    @Test
    fun `delete Movie From ShoppingCart`() {
        shoppingCartRepositoryImplementation.deleteMovieFromShoppingCart(movie.id)
        verify(shoppingCartDao).deleteMovieFromShoppingCart(movie.id)
    }

    @Test
    fun `delete All Movies From ShoppingCart`() {
        shoppingCartRepositoryImplementation.deleteAllMoviesFromShoppingCart()
        verify(shoppingCartDao).deleteAllMoviesFromShoppingCart()
    }
}