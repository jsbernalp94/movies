package co.jonathanbernal.movies.di

import android.app.Application
import android.content.Context
import co.jonathanbernal.movies.module.DatabaseModule
import co.jonathanbernal.movies.module.NetworkModule
import co.jonathanbernal.movies.module.RepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        ActivitiesBuilder::class,
        DatabaseModule::class]
)
@Singleton
interface AppComponent : AndroidInjector<MovieApplication> {

    fun context():Context
    fun application():Application

    @Component.Builder
    interface Builder{

        fun build():AppComponent

        @BindsInstance
        fun application(application: MovieApplication):Builder
    }
}