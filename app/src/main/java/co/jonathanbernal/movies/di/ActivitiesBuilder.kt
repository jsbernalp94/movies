package co.jonathanbernal.movies.di

import co.jonathanbernal.movie_detail.di.MovieDetailModule
import co.jonathanbernal.movies.MainActivity
import co.jonathanbernalmovies.movies.di.MoviesModule
import co.jonathanbernalshopping.shopping_cart.di.ShoppingCartModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesBuilder {

    @ContributesAndroidInjector(modules = [MoviesModule::class, MovieDetailModule::class, ShoppingCartModule::class])
    abstract fun bindMainActivity():MainActivity

}