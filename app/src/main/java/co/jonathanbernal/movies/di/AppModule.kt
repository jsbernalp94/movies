package co.jonathanbernal.movies.di

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import co.jonathanbernal.movie_detail.viewmodel.MovieDetailViewModelFactory
import co.jonathanbernalcommon.common.utils.SharedPreferences
import co.jonathanbernalmovies.movies.viewmodel.MoviesViewModelFactory
import co.jonathanbernalshopping.shopping_cart.viewmodel.ShoppingCartViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {

    @Singleton
    @Provides
    @JvmStatic
    fun provideContext(application: MovieApplication): Context = application.applicationContext

    @Singleton
    @Provides
    @JvmStatic
    fun provideApplication(application: MovieApplication): Application = application

    @Singleton
    @Provides
    @JvmStatic
    fun provideSharedPreferences(context: Context): SharedPreferences = SharedPreferences(context,"MoviePreferences")


    @Provides
    @Singleton
    @JvmStatic
    fun provideMoviesViewModelFactory(factory: MoviesViewModelFactory): ViewModelProvider.Factory = factory

    @Provides
    @Singleton
    @JvmStatic
    fun provideMovieDetailViewModelFactory(factory: MovieDetailViewModelFactory): ViewModelProvider.Factory = factory

    @Provides
    @Singleton
    @JvmStatic
    fun provideShoppingCarViewModelFactory(factory: ShoppingCartViewModelFactory): ViewModelProvider.Factory = factory

}