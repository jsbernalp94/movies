package co.jonathanbernal.movies.Interceptor

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val token = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI2Zjk4Mjg5OTIxMWNlNTZmYmU1MDg0OWFjMzIwYzI0YiIsInN1YiI6I" +
                "jYwM2E4MzM5NGJmYTU0MDA1MGI4ODE3MiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.GvwVk7oMGSFaVNsN" +
                "iw2HsdoKNZnQM26zdKmuLjq6EcA"
        if (token.isNotEmpty()) {
            val requestBuilder = original.newBuilder()
                .header("Authorization", "Bearer $token")
                .method(original.method, original.body)
            val request = requestBuilder.build()
            return chain.proceed(request)
        }

        return chain.proceed(chain.request())

    }

}