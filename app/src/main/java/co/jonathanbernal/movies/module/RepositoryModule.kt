package co.jonathanbernal.movies.module

import co.jonathanbernal.data.endpoint.MovieApi
import co.jonathanbernal.data.local.dao.MovieDao
import co.jonathanbernal.data.local.dao.ShoppingCartDao
import co.jonathanbernal.data.repository.MovieRepositoryImplementation
import co.jonathanbernal.data.mapper.MovieMapper
import co.jonathanbernal.data.repository.ShoppingCartRepositoryImplementation
import co.jonathanbernal.domain.repository.IMovieRepository
import co.jonathanbernal.domain.repository.IShoppingCartRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(movieApi: MovieApi, movieMapper: MovieMapper, movieDao: MovieDao):IMovieRepository{
        return MovieRepositoryImplementation(movieApi,movieMapper,movieDao)
    }

    @Provides
    @Singleton
    fun provideShoppingCartRepository(shoppingCartDao: ShoppingCartDao):IShoppingCartRepository{
        return ShoppingCartRepositoryImplementation(shoppingCartDao)
    }


}