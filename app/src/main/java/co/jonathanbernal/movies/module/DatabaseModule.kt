package co.jonathanbernal.movies.module

import androidx.room.Room
import co.jonathanbernal.data.local.dao.MovieDao
import co.jonathanbernal.data.local.dao.ShoppingCartDao
import co.jonathanbernal.data.local.database.MovieDatabase
import co.jonathanbernal.movies.di.MovieApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideMovieDatabase(application: MovieApplication): MovieDatabase = Room.databaseBuilder(application,
        MovieDatabase::class.java,"movie_database")
        .build()

    @Provides
    @Singleton
    fun providesMovieDao(database: MovieDatabase): MovieDao = database.movieDao()

    @Provides
    @Singleton
    fun providesShoppingCarDao(database: MovieDatabase): ShoppingCartDao = database.shoppingCartDao()
}