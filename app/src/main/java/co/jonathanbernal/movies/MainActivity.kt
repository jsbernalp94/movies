package co.jonathanbernal.movies

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import co.jonathanbernalcommon.common.utils.toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        navView.setupWithNavController(navController)
        val uri : Uri? = intent.data
        uri?.let { handleDeepLink(navController, it) }

    }

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    fun handleDeepLink(navController: NavController, uri: Uri) {
        uri.path?.let {
            val path = it.substring(1)
            val isNumeric = path.matches("-?\\d+(\\.\\d+)?".toRegex())
            if(isNumeric){
                val bundle = bundleOf("idMovie" to path.toInt())
                navController.navigate(R.id.action_navigation_movies_to_navigation_movie_detail, bundle)
            }else{
                this@MainActivity.toast(getString(R.string.error_deeplink_link,uri))
            }
        }
    }
}