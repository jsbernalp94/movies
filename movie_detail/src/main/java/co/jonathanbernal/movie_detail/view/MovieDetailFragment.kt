package co.jonathanbernal.movie_detail.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.jonathanbernal.movie_detail.R
import co.jonathanbernal.movie_detail.databinding.FragmentMovieDetailBinding
import co.jonathanbernal.movie_detail.viewmodel.MovieDetailViewModel
import co.jonathanbernal.movie_detail.viewmodel.MovieDetailViewModelFactory
import co.jonathanbernalcommon.common.utils.toast
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class MovieDetailFragment : Fragment() {

    @Inject
    lateinit var movieDetailViewModelFactory: MovieDetailViewModelFactory

    @Inject
    lateinit var movieDetailViewModel: MovieDetailViewModel

    lateinit var binding: FragmentMovieDetailBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_movie_detail,container,false)
        return binding.root
    }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            findNavController().navigate(getActionNavigation(findNavController().currentDestination!!.id))
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AndroidSupportInjection.inject(this)
        movieDetailViewModel = ViewModelProvider(this,movieDetailViewModelFactory).get(movieDetailViewModel::class.java)
        val idMovie = arguments?.getInt("idMovie")
        idMovie?.let { movieDetailViewModel.getMovieDetail(it) }
        binding.movieDetail = movieDetailViewModel

        movieDetailViewModel.error.observe(viewLifecycleOwner,{
            context?.toast(it)
            findNavController().navigate(R.id.action_navigation_movie_detail_to_navigation_movies)
        })

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )
    }

    private fun getActionNavigation(id: Int): Int {
        return when (id) {
            R.id.navigation_movies -> {
                R.id.action_navigation_movie_detail_to_navigation_movies
            }
            R.id.navigation_shopping_car -> {
                R.id.action_navigation_movie_detail_to_navigation_shopping_car
            }
            else -> {
                R.id.action_navigation_movie_detail_to_navigation_movies
            }

        }
    }



}