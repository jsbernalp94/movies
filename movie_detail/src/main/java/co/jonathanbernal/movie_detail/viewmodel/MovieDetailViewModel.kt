package co.jonathanbernal.movie_detail.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.jonathanbernal.domain.entities.Movie
import co.jonathanbernal.domain.usecase.delete.shoppingCar.DeleteShoppingCartUseCase
import co.jonathanbernal.domain.usecase.get.movie.GetMovieUseCase
import co.jonathanbernal.domain.usecase.insert.shopping_car.InsertShoppingCarUseCase
import co.jonathanbernal.domain.utils.Response
import co.jonathanbernal.movie_detail.R
import co.jonathanbernalcommon.common.utils.addTo
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    private val getMovieUseCase: GetMovieUseCase,
    private val insertShoppingCarUseCase: InsertShoppingCarUseCase,
    private val deleteShoppingCartUseCase: DeleteShoppingCartUseCase
): ViewModel(){

    companion object {
        private val TAG = MovieDetailViewModel::class.java.simpleName
    }

    private val compositeDisposable = CompositeDisposable()
    val movie = ObservableField<Movie>()
    var error: MutableLiveData<Int> = MutableLiveData()

    fun getMovieDetail(idMovie: Int) {
        getMovieUseCase.getMovie(idMovie)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { handleMovieResponse(it) }
            .addTo(compositeDisposable)
    }

    fun insertMovieToShoppingCar(movie: Movie){
        Completable.fromAction {
            insertShoppingCarUseCase.insertMovieToShoppingCar(movie)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
            .addTo(compositeDisposable)
    }

    fun deleteMovieToShoppingCar(movie: Movie){
        Completable.fromAction {
            deleteShoppingCartUseCase.deleteMovieFromShoppingCart(movie.id)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
                .addTo(compositeDisposable)
    }


    private fun handleMovieResponse(resultMovie: Response) {
        when(resultMovie){
            is Response.Failure -> {
                error.postValue(R.string.error_detail_movie)
            }
            is Response.Success ->{
                movie.set(resultMovie.data as Movie )
            }
        }
    }


}