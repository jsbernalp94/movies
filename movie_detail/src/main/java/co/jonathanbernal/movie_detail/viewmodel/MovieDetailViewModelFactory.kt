package co.jonathanbernal.movie_detail.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class MovieDetailViewModelFactory @Inject constructor(private val movieDetailViewModel: MovieDetailViewModel) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(movieDetailViewModel::class.java)) {
            return movieDetailViewModel as T
        }
        throw IllegalArgumentException("uknown class name")
    }
}