package co.jonathanbernal.movie_detail.di

import co.jonathanbernal.movie_detail.view.MovieDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MovieDetailModule {

    @ContributesAndroidInjector
    abstract fun provideMovieDetailFragment():MovieDetailFragment

}