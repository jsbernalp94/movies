package co.jonathanbernalcommon.common.utils

import android.content.Context
import com.securepreferences.SecurePreferences


class SharedPreferences(context: Context, filename: String) {

    private val preferences = SecurePreferences(context, "", filename)

    fun setString(key: String, value: String) {
        preferences.edit().putString(key, value).apply()
    }

    fun getString(key: String, defaultValue: String = ""): String {
        return preferences.getString(key, defaultValue).orEmpty()
    }

    fun setInt(key: String, value: Int) {
        preferences.edit().putInt(key, value).apply()
    }

    fun getInt(key: String, defaultValue: Int = 0): Int {
        return preferences.getInt(key, defaultValue)
    }

    fun setFloat(key: String, value: Float) {
        preferences.edit().putFloat(key, value).apply()
    }

    fun getFloat(key: String, defaultValue: Float = 0.0f): Float {
        return preferences.getFloat(key, defaultValue)
    }

    fun setBoolean(key: String, value: Boolean) {
        preferences.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
        return preferences.getBoolean(key, defaultValue)
    }

    fun contains(key: String): Boolean {
        return preferences.contains(key)
    }

    fun delete(key: String) {
        preferences.edit().remove(key).apply()
    }
}