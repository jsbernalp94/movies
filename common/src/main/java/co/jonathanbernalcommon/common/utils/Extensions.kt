package co.jonathanbernalcommon.common.utils

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun ImageView.loadMovieImage(path: String) {
    Glide
        .with(this.context)
        .load("https://image.tmdb.org/t/p/w500/$path")
        .into(this)
}


@JvmName("addToComposite")
fun Disposable.addTo(disposableComposite: CompositeDisposable) {
    disposableComposite.add(this)
}

fun View.setVisibility(isVisibility: Boolean) {
    this.visibility = if (isVisibility) View.VISIBLE else View.GONE
}

fun Context.toast(message:String) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()
fun Context.toast(message:Int) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()