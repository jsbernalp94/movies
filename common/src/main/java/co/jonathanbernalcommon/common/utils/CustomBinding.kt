package co.jonathanbernalcommon.common.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("loadimage")
fun bindingImage(imageView: ImageView, path: String?) {
    if (path != null) {
        imageView.loadMovieImage(path)
    }
}

@BindingAdapter("visibility")
fun setVisibilityBinding(view: View, visible: Boolean) {
    view.setVisibility(visible)
}