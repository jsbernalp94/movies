# README #

####  Requerimientos técnicos para construir el proyecto.


En primer lugar, se debe clonar el proyecto ejecutando el siguiente comando.

git clone https://jsbernalp94@bitbucket.org/jsbernalp94/movies.git

Una vez realizado esto, automáticamente Android studio se encarga de descargar todas las dependencias necesarias para 
compilar el proyecto.

El resultado final se encuentra en la rama MASTER.




####  Descripción técnica para el funcionamiento del Deep link.

Existen 3 casos controlados en la app en lo relacionado a Deep Link.

2.1	En primer lugar, un link en el cual se ingresa al detalle y se muestra la información de una película existente 
en la base de datos local. 

https://jonathanbernal.co/664767 (página 1)


2.2	Por otro lado, un link de una película que no existe en la bd local, que probablemente aun no ha sido descargada. 


https://jonathanbernal.co/631939 (página 2)

https://jonathanbernal.co/351286 (página 15)


2.3	Finalmente, está el caso de un link que contiene letras y números, por lo cual, no ingresara al detalle del proceso 
e informara al usuario que el deep link es erróneo 

https://jonathanbernal.co/3m5t1s






####  Breve descripción de la responsabilidad de cada capa propuesta.

El desarrollo de la aplicación está basado en Clean Architecture, por lo cual se implementaron 3 capas:
-	Capa de datos: Se encuentra en el módulo “data” del proyecto, allí se encuentra el acceso a los datos remotos, es decir, 
			la conexión al api y de igual forma acceso a los datos locales, es decir, la base de datos de Room.
-	Dominio: Hace referencia al módulo “domain” del proyecto, allí se encuentra toda la lógica. De igual forma, se implementan 
		 los casos de uso para administrar las peticiones al repositorio.
-	Presentación: Hace referencia a los fragments junto con sus viewmodels, en este caso, los módulos de “movies”, “movie_detail” 
		      y “shopping_cart”.

Adicionalmente, en se implemento el modulo “common” en el cual se alojan las propiedades comunes de todos los modulos, como por ejemplo 
las extensions y sharedpreferences.
Finalmente, en el módulo principal “app”, se encuentra toda la configuración del proyecto como la inyección de dependencias con Dagger 
y es desde donde se ejecutará la aplicación y tendrá el acceso a todos los modulos.





####  Screenshots que muestren la lista de películas, el detalle de una película y el carrito.

4.1	Listado de Peliculas

En el primer inicio de la app, automáticamente se ejecuta la descarga de la primera página de películas del api. Cada pagina cuenta 
con 20 películas y una vez se llega al final de la misma, se realiza otra petición al api para obtener las películas de la pagina 
siguiente. De igual forma, la ultima pagina descargada se guarda en sharedPreferences, de tal forma que cuando el usuario cierre 
la app y vuelva a ingresar cuando llegue al final del listado, continúe descargando películas.

	https://i.postimg.cc/Hx2SQYh3/movie-list.png

El listado de películas cuenta con la opción de agregar y eliminar película al carrito.

4.2	Detalle de la película

Se puede ingresar al detalle desde el listado de películas y el carrito de compras. 

	https://i.postimg.cc/Kc1qK257/movie-detail.png
	https://i.postimg.cc/dQgWrxWb/movie-detail2.png

Allí, se encuentra un botón flotante que permite agregar o eliminar películas al carrito de compras.


4.3 Carrito de Compras

En esta pantalla, se pueden ver las películas que han sido agregadas desde el listado de películas o el detalle de la película. 
También, cuenta con la opción de eliminar para cada item y el botón flotante que permite eliminar todas las películas del carrito 
de compras

	https://i.postimg.cc/qvGVmTSh/shopping-cart.png
	https://i.postimg.cc/SKT0hGym/shopping-cart2.png
	https://i.postimg.cc/3wJMVqQL/shopping-cart3.png

