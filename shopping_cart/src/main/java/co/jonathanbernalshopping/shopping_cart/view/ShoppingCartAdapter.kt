package co.jonathanbernalshopping.shopping_cart.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import co.jonathanbernal.domain.entities.ShoppingCart
import co.jonathanbernalshopping.shopping_cart.viewmodel.ShoppingCartViewModel

class ShoppingCartAdapter internal constructor(var shoppingCartViewModel: ShoppingCartViewModel, var resource: Int) :
    RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder>() {

    private var movieListAdapter: List<ShoppingCart> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(layoutInflater, viewType, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setMoviesCard(shoppingCartViewModel, position)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    private fun getLayoutIdForPosition(position: Int): Int {
        return resource
    }

    override fun getItemCount(): Int {
        return movieListAdapter.size
    }

    fun setMovieList(movies: List<ShoppingCart>) {
        this.movieListAdapter = movies
        notifyDataSetChanged()
    }

    class ViewHolder(binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        private var binding: ViewDataBinding? = null

        init {
            this.binding = binding
        }

        fun setMoviesCard(shoppingCartViewModel: ShoppingCartViewModel, position: Int) {
            binding?.setVariable(BR.itemShoppingCar, shoppingCartViewModel)
            binding?.setVariable(BR.position, position)
            binding?.executePendingBindings()
        }

    }
}