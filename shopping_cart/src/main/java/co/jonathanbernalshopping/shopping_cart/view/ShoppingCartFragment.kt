package co.jonathanbernalshopping.shopping_cart.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import co.jonathanbernalcommon.common.utils.toast
import co.jonathanbernalshopping.shopping_cart.R
import co.jonathanbernalshopping.shopping_cart.databinding.FragmentShoppingCartBinding
import co.jonathanbernalshopping.shopping_cart.viewmodel.ShoppingCartViewModel
import co.jonathanbernalshopping.shopping_cart.viewmodel.ShoppingCartViewModelFactory
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_shopping_cart.*
import javax.inject.Inject


class ShoppingCartFragment : Fragment() {

    @Inject
    lateinit var shoppingCartViewModelFactory: ShoppingCartViewModelFactory

    @Inject
    lateinit var shoppingCartViewModel: ShoppingCartViewModel

    lateinit var binding : FragmentShoppingCartBinding



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_shopping_cart,container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AndroidSupportInjection.inject(this)
        shoppingCartViewModel = ViewModelProvider(this, shoppingCartViewModelFactory).get(shoppingCartViewModel::class.java)
        binding.shoppingCartList = shoppingCartViewModel
        shoppingCartViewModel.getShoppingCarListFromDb()

        shoppingCartViewModel.movies.observe(viewLifecycleOwner, {
            if (it.isEmpty()) {
                textview_empty_shopping_cart.visibility = View.VISIBLE
            } else {
                textview_empty_shopping_cart.visibility = View.GONE
            }
            shoppingCartViewModel.setData(it)
        })

        shoppingCartViewModel.idMovie.observe(viewLifecycleOwner,{ idMovie->
            val bundle = bundleOf("idMovie" to idMovie)
            findNavController().navigate(R.id.action_navigation_shopping_cart_to_navigation_movie_detail,bundle)
        })

        shoppingCartViewModel.error.observe(viewLifecycleOwner,{
            context?.toast(it)
        })

    }
}