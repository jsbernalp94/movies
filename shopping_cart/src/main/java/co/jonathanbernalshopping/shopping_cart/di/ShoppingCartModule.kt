package co.jonathanbernalshopping.shopping_cart.di

import co.jonathanbernalshopping.shopping_cart.view.ShoppingCartFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ShoppingCartModule {

    @ContributesAndroidInjector()
    abstract fun providesShoppingCartFragment(): ShoppingCartFragment
}