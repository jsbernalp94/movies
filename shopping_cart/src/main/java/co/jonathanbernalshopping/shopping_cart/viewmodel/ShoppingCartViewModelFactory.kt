package co.jonathanbernalshopping.shopping_cart.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class ShoppingCartViewModelFactory @Inject constructor(private val shoppingCartViewModel: ShoppingCartViewModel) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(shoppingCartViewModel::class.java)) {
            return shoppingCartViewModel as T
        }
        throw IllegalArgumentException("uknown class name")
    }
}