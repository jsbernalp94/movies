package co.jonathanbernalshopping.shopping_cart.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import co.jonathanbernal.domain.entities.ShoppingCart
import co.jonathanbernal.domain.usecase.delete.shoppingCar.DeleteShoppingCartUseCase
import co.jonathanbernal.domain.usecase.get.shoppingCar.GetShoppingCarUseCase
import co.jonathanbernal.domain.utils.Response
import co.jonathanbernalcommon.common.utils.addTo
import co.jonathanbernalshopping.shopping_cart.R
import co.jonathanbernalshopping.shopping_cart.view.ShoppingCartAdapter
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import javax.inject.Inject

class ShoppingCartViewModel @Inject constructor(
        private val getShoppingCarUseCase: GetShoppingCarUseCase,
        private val deleteShoppingCartUseCase: DeleteShoppingCartUseCase
) : ViewModel() {

    var movies: MutableLiveData<List<ShoppingCart>> = MutableLiveData()
    var idMovie: MutableLiveData<Int> = MutableLiveData()
    var error : MutableLiveData<Int> = MutableLiveData()
    var listShoppingCartAdapter: ShoppingCartAdapter? = null
    private val compositeDisposable = CompositeDisposable()

    companion object {
        private val TAG = ShoppingCartViewModel::class.java.simpleName
    }

    fun getShoppingCarListFromDb() {
        getShoppingCarUseCase.getMovieListFromShoppingCar()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { manageResponseFromDb(it) }
                .addTo(compositeDisposable)
    }

    fun deleteMovieFromShoppingCart(position: Int) {
        Completable.fromAction {
            val movie = (movies.value)?.get(position)
            movie?.let { deleteShoppingCartUseCase.deleteMovieFromShoppingCart(movie.id) }
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe().addTo(compositeDisposable)
    }


    private fun manageResponseFromDb(result: Response) {
        when (result) {
            is Response.Failure -> {
                error.postValue(R.string.error_shopping_cart)
            }
            is Response.Success -> movies.postValue(result.data as List<ShoppingCart>)
        }
    }

    fun setData(list: List<ShoppingCart>) {
        listShoppingCartAdapter?.setMovieList(list)
    }

    fun getMovie(position: Int): ShoppingCart? {
        val movies: MutableLiveData<List<ShoppingCart>> = movies
        return movies.value?.get(position)
    }

    fun openMovieDetail(position: Int) {
        idMovie.postValue(movies.value?.get(position)?.id)
    }

    fun getRecyclerShoppingCarAdapter(): ShoppingCartAdapter? {
        listShoppingCartAdapter = ShoppingCartAdapter(this, R.layout.item_shopping_cart)
        return listShoppingCartAdapter
    }

    fun deleteAllMovies(){
        Completable.fromAction {
            deleteShoppingCartUseCase.deleteAllMovies()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe().addTo(compositeDisposable)
    }


}